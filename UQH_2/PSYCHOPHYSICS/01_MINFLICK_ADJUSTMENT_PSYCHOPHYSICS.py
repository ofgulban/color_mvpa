#!/usr/bin/env pythoN
"""
INSTRUCTIONS

Use keyboard button 1 or 2 to increase decrease lightness.
Use keyboard button 3 to reset the lightness temporarily.
Use keyboard button 4 to accept adjustment and go to the next color.

Use keyboard button Esc or q to end the program.
BEWARE: The program will continue until pressing Esc or q.

Please read the comments in the section below for further information.

@author: Omer Faruk Gulban
"""

from psychopy import core, visual, event, misc, monitors
import numpy as np
import copy
import colorsys
import time

#%%----------------------------------------------------------------------------
"""PLEASE_CHECK_THESE_VALUES"""

subjectID = 'Test' 	#enter the name of the participnat
runNumber = '00'	#enter the number of the run

fullScreen = False  #change to True for fullscreen

stimSize = 5        #stimulus size, degrees of visual angle 
stimPosX = 6        #stimulus distance from the center of the screen, degrees of visual angle
stepSize = 0.01     #determines how fast the lightness changes after each keypress
temporalFreq  = 18  #flickering speed
interTrialInt = 0.1 #seconds (inter trial interval)

#variables to calculate visual angle degrees
screenWidth = 30 #cm
eyeToScreen = 70 #cm

#initial colors
RE = [1.0, 0.0, 0.0] #red     [R, G, B]
GR = [0.0, 1.0, 0.0] #green   [R, G, B]
BL = [0.0, 0.0, 1.0] #blue    [R, G, B]
YE = [1.0, 1.0, 0.0] #yellow  [R, G, B]
CY = [0.0, 1.0, 1.0] #cyan    [R, G, B]
MA = [1.0, 0.0, 1.0] #magenta [R, G, B]

scriptID  = 'MINFLICK_ADJUSTMENT_PSYCHOPHYSICS'
projectID = 'COLOR_MVPA_UQH_2'

#%%----------------------------------------------------------------------------

date = time.strftime("%b_%d_%Y_%H_%M", time.localtime())

#loop params
loopDur = 1.0/temporalFreq #one loop (red gratings and grey gratings)
stimDur = loopDur/2

#create a window to draw in
moni = monitors.Monitor('testMonitor', width=screenWidth, distance=eyeToScreen)

myWin = visual.Window(size =(1024, 768),
                      fullscr = fullScreen,
                      screen = 0,
                      allowGUI = False,
                      allowStencil = False,
                      monitor = moni,
                      color = 'gray',
                      colorSpace='rgb')

#fixation dot

fixDot = visual.GratingStim(myWin, tex='none', mask='none', units='deg',
                            pos=(0.0, 0.0),
                            size=0.1,
                            color='black',
                            )
fixDot.setAutoDraw(True) #draws in each frame

#for psychopy special RGB
def rgb_to_rgbPsychopy (inputRGB):
    return (inputRGB[0]*2-1), (inputRGB[1]*2-1), (inputRGB[2]*2-1)

def updateTex (hlsList):
    return np.array([[rgb_to_rgbPsychopy(colorsys.hls_to_rgb(hlsList[0], hlsList[1], hlsList[2])), [0.0 ,0.0 ,0.0]]])

def hlsCheck (value):
    if value < 0:
        value = 0
    if value > 1:
        value = 1
    return value

#rgb to hls transformation
hlsRe = colorsys.rgb_to_hls(RE[0], RE[1], RE[2])
hlsGr = colorsys.rgb_to_hls(GR[0], GR[1], GR[2])
hlsBl = colorsys.rgb_to_hls(BL[0], BL[1], BL[2])
hlsYe = colorsys.rgb_to_hls(YE[0], YE[1], YE[2])

hlsCy = colorsys.rgb_to_hls(CY[0], CY[1], CY[2])
hlsMa = colorsys.rgb_to_hls(MA[0], MA[1], MA[2])


#making list for +/- when the buttons are pressed
hlsReList = [hlsRe[0],hlsRe[1],hlsRe[2]]
hlsGrList = [hlsGr[0],hlsGr[1],hlsGr[2]]
hlsBlList = [hlsBl[0],hlsBl[1],hlsBl[2]]
hlsYeList = [hlsYe[0],hlsYe[1],hlsYe[2]]
hlsCyList = [hlsCy[0],hlsCy[1],hlsCy[2]]
hlsMaList = [hlsMa[0],hlsMa[1],hlsMa[2]]

#hls to rgb transformation
rgbRe = colorsys.hls_to_rgb(hlsReList[0], hlsReList[1], hlsReList[2])
rgbGr = colorsys.hls_to_rgb(hlsGrList[0], hlsGrList[1], hlsGrList[2])
rgbBl = colorsys.hls_to_rgb(hlsBlList[0], hlsBlList[1], hlsBlList[2])
rgbYe = colorsys.hls_to_rgb(hlsYeList[0], hlsYeList[1], hlsYeList[2])
rgbCy = colorsys.hls_to_rgb(hlsCyList[0], hlsCyList[1], hlsCyList[2])
rgbMa = colorsys.hls_to_rgb(hlsMaList[0], hlsMaList[1], hlsMaList[2])

#psychopy special RGB transformation
S_rgbRe = rgb_to_rgbPsychopy(rgbRe) #S_ stands for SPECIAL
S_rgbGr = rgb_to_rgbPsychopy(rgbGr)
S_rgbBl = rgb_to_rgbPsychopy(rgbBl)
S_rgbYe = rgb_to_rgbPsychopy(rgbYe)
S_rgbCy = rgb_to_rgbPsychopy(rgbCy)
S_rgbMa = rgb_to_rgbPsychopy(rgbMa)


#grating texture
texRe = np.array([[S_rgbRe, [0.0 ,0.0 ,0.0]]])
texGr = np.array([[S_rgbGr, [0.0 ,0.0 ,0.0]]])
texBl = np.array([[S_rgbBl, [0.0 ,0.0 ,0.0]]])
texYe = np.array([[S_rgbYe, [0.0 ,0.0 ,0.0]]])
texCy = np.array([[S_rgbCy, [0.0 ,0.0 ,0.0]]])
texMa = np.array([[S_rgbMa, [0.0 ,0.0 ,0.0]]])

#save initial texture colors
texDefRe = np.array(copy.deepcopy(texRe))
texDefGr = np.array(copy.deepcopy(texGr))
texDefBl = np.array(copy.deepcopy(texBl))
texDefYe = np.array(copy.deepcopy(texYe))
texDefCy = np.array(copy.deepcopy(texCy))
texDefMa = np.array(copy.deepcopy(texMa))

#init variables
stimState = 0
prevState = -1
colorResults = np.zeros((6,1))
interruption = False
resultState = True

rgbReMat = []
rgbBlMat = []
rgbGrMat = []
rgbYeMat = []
rgbCyMat = []
rgbMaMat = []

#create a clock
clock = core.Clock()
clock.reset()

while True:

    if stimState == 0:
        #red
        stimReL = visual.GratingStim(myWin, tex=texRe, units='deg', size=stimSize,  pos=(-stimPosX, 0.0), colorSpace='rgb')
        stimReR = visual.GratingStim(myWin, tex=texRe, units='deg', size=stimSize,  pos=( stimPosX, 0.0), colorSpace='rgb')
        stim1 = stimReL
        stim2 = stimReR

    elif stimState == 1:
        #green
        stimGrL = visual.GratingStim(myWin, tex=texGr, units='deg', size=stimSize,  pos=(-stimPosX, 0.0), colorSpace='rgb')
        stimGrR = visual.GratingStim(myWin, tex=texGr, units='deg', size=stimSize,  pos=( stimPosX, 0.0), colorSpace='rgb')
        stim1 = stimGrL
        stim2 = stimGrR

    elif stimState == 2:
        #blue
        stimBlL = visual.GratingStim(myWin, tex=texBl, units='deg', size=stimSize,  pos=(-stimPosX, 0.0), colorSpace='rgb')
        stimBlR = visual.GratingStim(myWin, tex=texBl, units='deg', size=stimSize,  pos=( stimPosX, 0.0), colorSpace='rgb')
        stim1 = stimBlL
        stim2 = stimBlR

    elif stimState == 3:
        #yellow
        stimYeL = visual.GratingStim(myWin, tex=texYe, units='deg', size=stimSize,  pos=(-stimPosX, 0.0), colorSpace='rgb')
        stimYeR = visual.GratingStim(myWin, tex=texYe, units='deg', size=stimSize,  pos=( stimPosX, 0.0), colorSpace='rgb')
        stim1 = stimYeL
        stim2 = stimYeR

    elif stimState == 4:
        #cyan
        stimCyL = visual.GratingStim(myWin, tex=texCy, units='deg', size=stimSize,  pos=(-stimPosX, 0.0), colorSpace='rgb')
        stimCyR = visual.GratingStim(myWin, tex=texCy, units='deg', size=stimSize,  pos=( stimPosX, 0.0), colorSpace='rgb')
        stim1 = stimCyL
        stim2 = stimCyR

    elif stimState == 5:
        #magenta
        stimMaL = visual.GratingStim(myWin, tex=texMa, units='deg', size=stimSize,  pos=(-stimPosX, 0.0), colorSpace='rgb')
        stimMaR = visual.GratingStim(myWin, tex=texMa, units='deg', size=stimSize,  pos=( stimPosX, 0.0), colorSpace='rgb')
        stim1 = stimMaL
        stim2 = stimMaR
        
        #TODO: Bad solution for getting results output.
        resultState = True        
        
    elif stimState == 6: #for holding the results
    
        if resultState == True:
            #hls to rgb transformation
            rgbRe = colorsys.hls_to_rgb(hlsReList[0], hlsReList[1], hlsReList[2])
            rgbReMat.append(rgbRe)
            #psychopy SPECIAL RGB transformation
            S_rgbRe = rgb_to_rgbPsychopy(rgbRe) #S_ stands for psychopy's special RGB
    
            rgbGr = colorsys.hls_to_rgb(hlsGrList[0], hlsGrList[1], hlsGrList[2])
            rgbGrMat.append(rgbGr)
            S_rgbGr = rgb_to_rgbPsychopy(rgbGr)
    
            rgbBl = colorsys.hls_to_rgb(hlsBlList[0], hlsBlList[1], hlsBlList[2])
            rgbBlMat.append(rgbBl)
            S_rgbBl = rgb_to_rgbPsychopy(rgbBl)
    
            rgbYe = colorsys.hls_to_rgb(hlsYeList[0], hlsYeList[1], hlsYeList[2])
            rgbYeMat.append(rgbYe)
            S_rgbYe = rgb_to_rgbPsychopy(rgbYe)
    
            rgbCy = colorsys.hls_to_rgb(hlsCyList[0], hlsCyList[1], hlsCyList[2])
            rgbCyMat.append(rgbCy)
            S_rgbCy = rgb_to_rgbPsychopy(rgbCy)
    
            rgbMa = colorsys.hls_to_rgb(hlsMaList[0], hlsMaList[1], hlsMaList[2])
            rgbMaMat.append(rgbMa)
            S_rgbMa = rgb_to_rgbPsychopy(rgbMa)
            
            resultState = False
        

    #01_accurate timer
    if clock.getTime()%loopDur < stimDur:
        state = 1
    if stimDur < clock.getTime()%loopDur < stimDur*2:
        state = 2

    #02_limit frame flips
    if state != prevState:
        myWin.clearBuffer()
        if state == 1 and stimState < 6:
            stim1.draw()
            stim2.draw()

        if state == 2 and stimState < 6:
            stim1.setPhase(0.5)
            stim2.setPhase(0.5)
            stim1.draw()
            stim2.draw()

        myWin.flip()
        prevState = state

    #handle keypresses each frame
    for keys in event.getKeys(timeStamped=True):

        #change luminance
        if keys[0] in ['1']:

            if stimState == 0:
                hlsReList[1] += stepSize
                hlsReList[1] = hlsCheck(hlsReList[1])
                texRe = updateTex (hlsReList)
                stim1 = stimReL
                stim2 = stimReR

            elif stimState == 1:
                hlsGrList[1] += stepSize
                hlsGrList[1] = hlsCheck(hlsGrList[1])
                texGr = updateTex (hlsGrList)
                stim1 = stimGrL
                stim2 = stimGrR

            elif stimState == 2:
                hlsBlList[1] += stepSize
                hlsBlList[1] = hlsCheck(hlsBlList[1])
                texBl = updateTex (hlsBlList)
                stim1 = stimBlL
                stim2 = stimBlR

            elif stimState == 3:
                hlsYeList[1] += stepSize
                hlsYeList[1] = hlsCheck(hlsYeList[1])
                texYe = updateTex (hlsYeList)
                stim1 = stimYeL
                stim2 = stimYeR

            elif stimState == 4:
                hlsCyList[1] += stepSize
                hlsCyList[1] = hlsCheck(hlsCyList[1])
                texCy = updateTex (hlsCyList)
                stim1 = stimCyL
                stim2 = stimCyR

            elif stimState == 5:
                hlsMaList[1] += stepSize
                hlsMaList[1] = hlsCheck(hlsMaList[1])
                texMa = updateTex (hlsMaList)
                stim1 = stimMaL
                stim2 = stimMaR

        if keys[0] in ['2']:

            if stimState == 0:
                hlsReList[1] -= stepSize
                hlsReList[1] = hlsCheck(hlsReList[1])
                texRe = updateTex (hlsReList)
                stim1 = stimReL
                stim2 = stimReR

            elif stimState == 1:
                hlsGrList[1] -= stepSize
                hlsGrList[1] = hlsCheck(hlsGrList[1])
                texGr = updateTex (hlsGrList)
                stim1 = stimGrL
                stim2 = stimGrR

            elif stimState == 2:
                hlsBlList[1] -= stepSize
                hlsBlList[1] = hlsCheck(hlsBlList[1])
                texBl = updateTex (hlsBlList)
                stim1 = stimBlL
                stim2 = stimBlR

            elif stimState == 3:
                hlsYeList[1] -= stepSize
                hlsYeList[1] = hlsCheck(hlsYeList[1])
                texYe = updateTex (hlsYeList)
                stim1 = stimYeL
                stim2 = stimYeR

            elif stimState == 4:
                hlsCyList[1] -= stepSize
                hlsCyList[1] = hlsCheck(hlsCyList[1])
                texCy = updateTex (hlsCyList)
                stim1 = stimCyL
                stim2 = stimCyR

            elif stimState == 5:
                hlsMaList[1] -= stepSize
                hlsMaList[1] = hlsCheck(hlsMaList[1])
                texMa = updateTex (hlsMaList)
                stim1 = stimMaL
                stim2 = stimMaR

        if keys[0] in ['3']:

            if stimState == 0:
                texRe = copy.deepcopy(texDefRe)
                stim1 = stimReL
                stim2 = stimReR

            elif stimState == 1:
                texGr = copy.deepcopy(texDefGr)
                stim1 = stimGrL
                stim2 = stimGrR

            elif stimState == 2:
                texBl = copy.deepcopy(texDefBl)
                stim1 = stimBlL
                stim2 = stimBlR

            elif stimState == 3:
                texYe = copy.deepcopy(texDefYe)
                stim1 = stimYeL
                stim2 = stimYeR

            elif stimState == 4:
                texCy = copy.deepcopy(texDefCy)
                stim1 = stimCyL
                stim2 = stimCyR

            elif stimState == 5:
                texMa = copy.deepcopy(texDefMa)
                stim1 = stimMaL
                stim2 = stimMaR

        if keys[0] in ['4']:
            stimState = (stimState + 1)%7
            myWin.clearBuffer()
            myWin.flip()
            core.wait(interTrialInt)

        #to quit
        if keys[0] in ['escape','q']:
            equilumColors = {'Initial_Red'     :RE,
                             'Initial_Green'   :GR,
                             'Initial_Blue'    :BL,
                             'Initial_Yellow'  :YE,
                             'Initial_Cyan'    :CY,
                             'Initial_Magenta' :MA,
                             'equiRed'         :rgbReMat,
                             'equiGreen'       :rgbGrMat,
                             'equiBlue'        :rgbBlMat,
                             'equiYellow'      :rgbYeMat,
                             'equiCyan'        :rgbCyMat,
                             'equiMagenta'     :rgbMaMat,                        
                             'ProjectID'       :projectID,
                             'ScriptID'        :scriptID,
                             'Date'            :date,
                             'SubjectID'       :subjectID,
                             'Run_Number'      :runNumber,
                             'Stimulus_Size'   :stimSize,
                             'Temporal_Frequency':temporalFreq,
                             'Screen_Width'    :screenWidth,
                             'Screen_Distance' :eyeToScreen
                             }
            interruption = True
            break
        
    if interruption:
        print '!!! \nInterrupted! \n!!! '
        break

#name of the output file
outFileName = subjectID +'-'+ scriptID +'-'+ runNumber +'-'+ date
misc.toFile(outFileName + '.pickle', equilumColors)

print '---'
print 'Adjusted colors saved as ' + outFileName + '.pickle'
print '---'
print 'Finished in %i seconds ' %clock.getTime()
print '---'

myWin.close()
core.quit()