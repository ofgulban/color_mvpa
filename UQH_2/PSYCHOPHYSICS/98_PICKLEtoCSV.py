# -*- coding: utf-8 -*-
"""
Simple .pickle reader.
This script is going to used to create .csv files.

@author: Omer Faruk Gulban
"""

from psychopy import misc
from pprint import pprint
from colorsys import rgb_to_hls
import glob
import csv

#%%****************

outputFileName = 'CSV_OUTPUT_TEST.csv'

#%%****************

#find pickle filenames
inputFileNames = glob.glob("ESLI\*.pickle")

pprint(inputFileNames)
print '---\n Nr. of files: %i \n---' %len(inputFileNames)

#append all pickles to a list
inPickles = []

for i in range(0,len(inputFileNames)):
    
    pickle = misc.fromFile(inputFileNames[i])
    inPickles.append([pickle])

#print the first pickle as an example
pprint(inPickles[:])

#%%

#Create .csv file

with open(outputFileName, 'w') as fp:
    a = csv.writer(fp, delimiter=',', lineterminator='\n')

    a.writerow(['PartID',
                'Re[hLs]','Gr[hLs]','Bl[hLs]','Ye[hLs]','Cy[hLs]','Ma[hLs]',
                'Re(HLS)','Gr(HLS)','Bl(HLS)','Ye(HLS)','Cy(HLS)','Ma(HLS)'
                'Re(RGB)','Gr(RGB)','Bl(RGB)','Ye(RGB)','Cy(RGB)','Ma(RGB)'
                ])

    for i in range(0,len(inputFileNames)):

        partID = inPickles[i][0]['SubjectID']
    
        rgbEquiRe    = inPickles[i][0]['equiRed'][-1]
        rgbEquiGr    = inPickles[i][0]['equiGreen'][-1]
        rgbEquiBl    = inPickles[i][0]['equiBlue'][-1]
        rgbEquiYe    = inPickles[i][0]['equiYellow'][-1]
        rgbEquiCy    = inPickles[i][0]['equiCyan'][-1]
        rgbEquiMa    = inPickles[i][0]['equiMagenta'][-1]

        hlsEquiRe = rgb_to_hls(rgbEquiRe[0], rgbEquiRe[1], rgbEquiRe[2])
        hlsEquiGr = rgb_to_hls(rgbEquiGr[0], rgbEquiGr[1], rgbEquiGr[2])
        hlsEquiBl = rgb_to_hls(rgbEquiBl[0], rgbEquiBl[1], rgbEquiBl[2])
        hlsEquiYe = rgb_to_hls(rgbEquiYe[0], rgbEquiYe[1], rgbEquiYe[2])
        hlsEquiCy = rgb_to_hls(rgbEquiCy[0], rgbEquiCy[1], rgbEquiCy[2])
        hlsEquiMa = rgb_to_hls(rgbEquiMa[0], rgbEquiMa[1], rgbEquiMa[2])
        
        a.writerow([partID, 
                    hlsEquiRe[1], hlsEquiGr[1],hlsEquiBl[1],
                    hlsEquiYe[1], hlsEquiCy[1],hlsEquiMa[1],
                    hlsEquiRe, hlsEquiGr, hlsEquiBl, #hls values for quick reference
                    hlsEquiYe, hlsEquiCy, hlsEquiMa,
                    rgbEquiRe, rgbEquiGr, rgbEquiBl, #rgb values for quick reference
                    rgbEquiYe, rgbEquiCy, rgbEquiMa,
                    ])

print '---\n Finished \n---'
