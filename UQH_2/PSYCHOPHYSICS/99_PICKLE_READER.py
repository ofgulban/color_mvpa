# -*- coding: utf-8 -*-
"""
Simple .pickle reader.
Uses Psychopy.
This script is going to used to create .txt files in desired formats.

@author: Omer Faruk Gulban
"""

from psychopy import misc
from pprint import pprint

#%%
"""Please enter the exact name of the pickle file"""
inputFileName = 'example.pickle'

#%%
try:
    pickle = misc.fromFile(inputFileName)
except:
    print '!!!\nFile cannot be read!\n!!!'

#%% some easy to use notes

pprint(pickle)

