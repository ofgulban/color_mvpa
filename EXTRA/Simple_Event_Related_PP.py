#author:Omer_Faruk_Gulban
#!/usr/bin/env python
from psychopy import visual, event, core, monitors, data

#set monitor information used in the experimental setup
MONITOR = monitors.Monitor('UMRAM', width=70, distance=130) #centimeters

#set screen (make 'fullscr = True' for fullscreen)
myWin = visual.Window(size =(1024, 768), fullscr = False, winType='pyglet', 
                      screen = 0, allowGUI = False, allowStencil = False,
                      monitor = MONITOR, color = 'gray', colorSpace='rgb')

#load stimulus set from a .csv file                      
stimList = data.importConditions('stimSet.csv')                      

#organise the stimuli with the trial handler
trials = data.TrialHandler(stimList, nReps=1, method='random')
trials.data.addDataType('Response')#this will help store things with the stimuli
trials.data.addDataType('RT')#add as many types as you like

#fixation dot
fixDot = visual.GratingStim(myWin, tex='none', mask='none', units='deg', pos=(0.0, 0.0), 
    size=0.05, sf=None, color='white', depth=1 )
fixDot.setAutoDraw(False) #draws for each frame

#create some watches
stopWatch = core.Clock()
eventWatch = core.Clock()

#parametres
OFFSET_DUR  = 3 #seconds, initial offset after trigger
Q2R_DUR = 0.5 #the time between keypress and initiation of response phase
STIM_DUR    = 0.200 #seconds, duration of the words
EVENT_DUR   = 5 #seconds, duration of words+rest presentation
MAX_RESP_WAIT = 3 #seconds, maximum wait duration to record response


nDone = 0 #used in for loop to print
eventPass = True
state=1

#initiate the experiment
myWin.flip() #one flip to guarantee screen preparation before trigger signal

#wait for the trigger signal
trigger = event.waitKeys(keyList=['6']) #wait until the key signal arrives

stopWatch.reset() #reset time after trigger

while stopWatch.getTime() < OFFSET_DUR:
    fixDot.draw()
    myWin.flip() #draw fixation dot during the offset period

else:
    for thisTrial in trials: #handler can act like a "for loop" 
        
        #select & draw the stimuli        
        questionStim = visual.TextStim(myWin, text=thisTrial['questions'])         
        questionStim.draw()
        myWin.flip()
        event.waitKeys(keyList=['space'])

        stopWatch.reset()
        myWin.flip()        
        while stopWatch.getTime() < Q2R_DUR: #wait until total stimulus duration
            pass
        
        #reset timers
        eventWatch.reset()
        stopWatch.reset()        
        
        #prepare stimuli of this trial
        textStim = visual.TextStim(myWin, text=thisTrial['words'])
        textStim.draw()
        myWin.flip() #should be frame rate controlled
        
        while stopWatch.getTime() < STIM_DUR: #wait until total stimulus duration
            pass        
        #clear events & buffer
        event.clearEvents()            
        myWin.clearBuffer()

        #draw fixation dot in response phase
        fixDot.draw()
        myWin.flip()
        
        keyList = event.waitKeys(maxWait=MAX_RESP_WAIT, #wait in seconds
                                 keyList=['c','m']) #'c' is yes, 'm' is no
        thisReactionTime = stopWatch.getTime()
        try:
            thisKey = keyList[0]   # --> holds nothing, 'c' or 'm'
        except:
            thisKey = '_NR_'  # nothing --> _NR_      
        
        trials.data.add('RT', thisReactionTime) #add reaction time data to our set
        trials.data.add('Response', thisKey) #add response key data to our set

        nDone += 1  #just for a quick reference
        print 'trial %i had position %s in the list (word=%s)' \
              %(nDone, trials.thisIndex, thisTrial['words'])
        
        while  stopWatch.getTime() < EVENT_DUR: #wait until total event duration
            pass
                
#after the experiment
print '\n'
trials.printAsText(stimOut=['words'], #write summary data to screen 
                   dataOut=['n_raw', 'RT_raw', 'Response_raw', 'order_raw'])
trials.saveAsText(fileName='testData', # also write summary data to a text file
                  stimOut=['words'], 
                  dataOut=['n_raw', 'RT_raw', 'Response_raw', 'order_raw'])
print '' #just an empty line

#close
myWin.close()
core.quit()