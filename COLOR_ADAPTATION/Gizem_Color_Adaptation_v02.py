# -*- coding: utf-8 -*-
"""
Minimal frame.
"""

#!/usr/bin/env python GO TO 79. LINE TO CHANGE THE BACKGROUND COLOR
from psychopy import visual, monitors, core, event
import numpy as np

#%%
"""MONITOR"""

# set monitor information used in the experimental setup
moni = monitors.Monitor('testMonitor', width=8.2, distance=60) #cm,

# set screen (make 'fullscr = True' for fullscreen)
myWin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
                      
#%%
"""STIMULUS"""

gratingTexture0 = np.array([[[ 1, -1,-1], [-1, 1,-1]]]) #red green
gratingTexture1 = np.array([[[-1, -1,-1], [ 1, 1, 1]]]) #black white

#red-green grating
gratingLeft  = visual.GratingStim(myWin, mask = 'raisedCos', units = 'deg', 
                                  tex     = gratingTexture0,
                                  pos     = (-3, 0), 
                                  contrast= 1,
                                  size    = 3,
                                  sf      = 0.7, 
                                  colorSpace='rgb')

gratingRight = visual.GratingStim(myWin, mask = 'raisedCos', units = 'deg', 
                                  tex     = gratingTexture0,
                                  pos     = (3, 0), 
                                  contrast= 1,
                                  size    = 3,
                                  sf      = 0.7, 
                                  colorSpace='rgb')
                              
testText = visual.TextStim(win=myWin, color='red', height=0.2)


#%%
"""Scanner Trigger"""
scannerStartTrigger = True
while scannerStartTrigger:
    myWin.flip()
    
    for keys in event.getKeys():
        if keys in ['6']:
            scannerStartTrigger = False

        elif keys[0]in ['escape','q']:
            myWin.close()
            core.quit()

#%%
"""TIME"""

# params
totalTime = 28 #

# give the system time to settle
core.wait(0.5)

# create a clock
clock=core.Clock()
clock.reset()

#%%
"""RENDER_LOOP"""


randomNrSwitch = True
targetDetectionCount = 0

while clock.getTime()<totalTime:

    t = clock.getTime()
    testText.setText(clock.getTime())

    # random number handler
    if t%12<6 and randomNrSwitch: 
        randomNumber = np.random.randint(0,10)/10.
        randomNrSwitch = False

    if t%12>=6:
        randomNrSwitch = True

    # set drift direction related properties
    if t%1<randomNumber:
        gratingLeft.setOri(0)
        gratingRight.setOri(0)

    if t%1>=randomNumber:
        gratingLeft.setOri(180)
        gratingRight.setOri(180)
    

    # set contrast related properties
    if t%48<20:
        gratingLeft.setTex(gratingTexture0)
        gratingRight.setTex(gratingTexture0)
        
        gratingLeft.setContrast(1)
        gratingRight.setContrast(1)
        
        condtiton1Switch = True

    elif t%48<24:  
        gratingLeft.setTex(gratingTexture1)
        gratingRight.setTex(gratingTexture1)

        gratingLeft.setContrast(1)
        gratingRight.setContrast(1)


    elif t%48<44:
        gratingLeft.setTex(gratingTexture0)
        gratingRight.setTex(gratingTexture0)

        gratingLeft.setContrast(1)
        gratingRight.setContrast(1)
        
    elif t%48>=48:  
        gratingLeft.setTex(gratingTexture0)
        gratingRight.setTex(gratingTexture0)
        
        gratingLeft.setContrast(0.3)
        gratingRight.setContrast(0.3)

    #target flash handling
    if t%4<0.250 and condtiton1Switch:
        gratingLeft.setContrast(0.1)
        gratingRight.setContrast(0.1)
        targetDetectionSwitch = True

    if t%4>0.500:
        targetDetectionSwitch = False

    gratingLeft.setPhase(t*2)
    gratingRight.setPhase(t*2)
        
    gratingLeft.draw()
    gratingRight.draw()
    
    testText.setText(t%24)
    testText.draw()

    myWin.flip()
    
    #handle key presses each frame
    for keys in event.getKeys(timeStamped=True):

        if keys[0]in ['t'] and targetDetectionSwitch:
            targetDetectionCount = targetDetectionCount + 1
            
        if keys[0]in ['escape','q']:
            print 'Target detection count:'
            print targetDetectionCount
            myWin.close()
            core.quit()     

# prınt target detecttion count before closing eveythıng
print 'Target detection count:'
print targetDetectionCount

myWin.close()
core.quit()